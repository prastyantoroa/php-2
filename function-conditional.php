<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/

// Code function di sini
function greetings($string){
    echo "Halo $string Selamat Datang di Sanbercode! <br>";
}

// Hapus komentar untuk menjalankan code!
 greetings("Bagas");
 greetings("Wahyu");
 greetings("Abdul");

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini 
function reverse($kata){
    $length = strlen($kata);
    $tampung = "";
    for ($i=($length-1); $i >=0 ; $i--) {
        $tampung .=$kata[$i];        
    }
    return $tampung;
}
function reverseString($kata2){
    $string = reverse($kata2);
    echo $kata2 . " => " ;
    echo $string . "<br>";
}

// Hapus komentar di bawah ini untuk jalankan Code
 
 reverseString("abduh");
 reverseString("Sanbercode");
 reverseString("We Are Sanbers Developers");
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


// Code function di sini
function palindrome($kata3){
    $balik=reverse($kata3);
        if ($kata3 == $balik) {
            echo $kata3 . " => True <br>" ;
        }else{
            echo $kata3 . " => False <br>";
        }     
}

// Hapus komentar di bawah ini untuk jalankan code
 palindrome("civic") ; // true
 palindrome("nababan") ; // true
 palindrome("jambaban"); // false
 palindrome("racecar"); // true
 palindrome("kodok"); 
 palindrome("sepatu"); 


echo "<h3>Soal No 4 Tentukan Nilai ";
/*
Soal 4
buatlah sebuah function bernama tentukan-nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
function tentukan_nilai($number){
    if ($number >= 98 && $number <100) {
        return "Sangat Baik <br>";        
    } else if ($number >= 76 && $number <98) {
        return "Baik <br>";
    } else if ($number >= 67 && $number <76) {
        return "Cukup <br>";
    } else if ($number >= 43 && $number <67) {
        return "Kurang <br>";
    }else if ($number >= 0 && $number <43) {
        return "Kurang Sekali <br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
 echo "<br>";
 echo "Nilai : 98 s/d 100 => ";
 echo tentukan_nilai(98); //Sangat Baik
 echo "Nilai : 76 s/d 97 => ";
 echo tentukan_nilai(76); //Baik
 echo "Nilai : 67 s/d 75=> ";
 echo tentukan_nilai(67); //Cukup
 echo "Nilai : 43 s/d 66=> ";
 echo tentukan_nilai(43); //Kurang
 echo "Nilai : 0 s/d 42=> ";
 echo tentukan_nilai(0); //Kurang


?>

</body>

</html>